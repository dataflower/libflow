#ifndef LIBFLOW_GRAPH_H
#define LIBFLOW_GRAPH_H

#include <flow/component.h>

#include <map>
#include <string>

namespace flow {

  // A graph manages a set of components and their connections
  //
  // A graph can be used as a component in other graphs, and can be
  // used as a basic building block for the hierarchical decomposition
  // of systems

  class graph : public flow::component {

  public:

    explicit graph (const std::string & name);
    
    ~graph();

    void add_component (flow::component & component);
    void remove_component (const std::string &name);
    const component & get_component (const std::string &name);

    unsigned int num_components();

    bool connect (const std::string & src_port, 
		  const std::string & target_port);

    bool create_initial_packet (const std::string & target_port, 
				flow::packet data);

    virtual void process();

  private:

    std::map<const std::string, flow::component *> m_children;
    
    std::tuple<std::string, std::string> 
      parse_descriptor (const std::string &descriptor_str);
  };

} // namespace flow

#endif // LIBFLOW_GRAPH_H
