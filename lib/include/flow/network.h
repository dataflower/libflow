#ifndef LIBFLOW_NETWORK_H
#define LIBFLOW_NETWORK_H

#include <flow/graph.h>

namespace flow {

  class network {
  
  public:

    explicit network();
    
    ~network();

    void add (flow::graph & graph);
    
    void run();
  };

} // namespace flow

#endif // LIBFLOW_NETWORK_H
