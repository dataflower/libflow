#ifndef LIBFLOW_COMPONENT_H
#define LIBFLOW_COMPONENT_H

#include <flow/port.h>
#include <flow/packet.h>

#include <string>
#include <map>

namespace flow {

// A *Component* is a self contained unit of computation.
//
// Components encapsulate asynchronous units of computation, and are
// responsible for managing their own internal state. They should only
// communicate with the rest of the system via packets sent to and
// from their ports.
//
// Components are oblivious to where their ports are connected and
// might block when reading certain ports, if the port is empty. They
// can also block when writing to a port which is full.  To implement
// a component the user must at least implement the <process> method,
// which is called when the process is scheduled to do some work, and
// must also ovveride the constructor to register the component's
// ports and to perform any required state initialization.
//
// There are two types of component: continuous (it they are not
// expected to return from the <process> method until they stop) and
// on-demand (if they are expected to return from the <process> method
// after doing some processing).
//
// Components own both the packets they create and the packets they
// receive, and are responsible for either disposing of them or
// sending them to another process.

  class component {
  
  public:

    // Component states
    enum class state {ready, running, blocked, stopped};
    
    // Base component constructor
    component (const std::string & name);
    
    // Destructor
    ~component();

    // Disable copies
    component (const component &c) = delete;
    component & operator= (const component &) = delete;

    // Disable moves
    component (component &&c) = delete;
    component & operator= (component &&c) = delete;

    // Get the name if the component
    const std::string & get_name() const;

    // Get the current component state
    component::state get_state();

    // Get an input port by name
    input_port & get_input_port (const std::string &port_name);

    // Get an output port by name
    output_port & get_output_port(const std::string &port_name);

    // Components must override this method
    virtual void process() = 0;

    // Components can override this method
    virtual void cleanup() {};

    // Run component until stopped
    // FIXME: move into scheduler
    void run();

  protected:

    // Internal API used by component implementations

    // cldoc:begin-category(component-api)

    // Register an input port for the compoent
    void create_input (flow::input_port & port);
    
    // Register an output port for the component
    void create_output (flow::output_port & port);

    // Receive a packet from @port 
    flow::packet receive_packet (flow::input_port & port);
    
    // Send @packet to @port
    void send_packet (flow::output_port & port, flow::packet &packet);
    
    // Send an End-of-Stream packet to @port, signalling that the
    // component does no intend to use the port anymore
    void send_eos (flow::output_port & port);

    // Create a new packet from @data
    flow::packet create_packet (std::string &data);
    flow::packet create_packet (int data);
    flow::packet create_packet (float data);

    // Drop a packet after using it
    void drop_packet (flow::packet &packet);

    // Stop the component
    void stop();

    // cldoc:end-category()

  private:
    
    std::string m_name;
    component::state m_state;
    int m_packets;
    std::map<std::string, flow::input_port *> m_inputs;
    std::map<std::string, flow::output_port *> m_outputs;
  };

} // namespace flow

#endif // LIBFLOW_COMPONENT_H
