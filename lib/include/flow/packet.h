#ifndef LIBFLOW_PACKET_H
#define LIBFLOW_PACKET_H

#include <string>

namespace flow {

  union _pd;
  typedef union _pd packet_data;
  
  // A *packet* is a single unit of data to be processed.
  //   
  //  Packets are responsible for allocating their internal data and
  //  are owned by the processes that receives them. They must be
  //  either passed on to another process or explicitly destroyed by
  //  the owning process.
  //  
  //  There are three major packet types:
  //   * Data packets that function like tuples.
  //   * Bracket packets are used to delimit packet groups (called substreams).
  //   * End-of-Stream packets that signal that a process does not intend to 
  //     send any more data through the stream.
  //  
  //  Packets can contain other packets in a tree-like structure,
  //  called chains. 
  //

  class packet {
    
  public:

    // Packet type constants
    enum class type {data, group_start, group_end, eos};

    // Factory function to create a data packet
    static flow::packet create_data (const std::string & tag, void *data);
    
    // Factory function to create a group start (bracket) packet
    static flow::packet create_group_start (const std::string &tag = "",
					    void *data = nullptr);
    
    // Factory function to create a group end (bracket) packet
    static flow::packet create_group_end (const std::string &tag = "",
					  void * data = nullptr);
    
    // Factory function to create an EOS packet
    static flow::packet create_eos (const std::string &tag = "",
				    void * data = nullptr);

    // explicit constructor 
    explicit packet (packet::type type, const std::string &tag, void * data);

    // disable copies
    packet (const packet &p) = delete;
    packet & operator = (const packet &p) = delete;

    // enable moves
    packet (packet &&p);
    packet & operator = (packet &&p);

    // destructor
    ~packet();

    // get the packet type
    packet::type get_type();
    
    // get the packet type tag
    const std::string & get_tag();

    // get packet data as a string
    const std::string & get_str (size_t index);

    // get packet data as int
    int get_int (size_t index);

    // get packet data as float
    float get_float (size_t index);

  private:

    packet::type m_type;
    std::string m_tag;
    packet_data *mp_data;
  };

}

#endif // LIBFLOW_PACKET_H
