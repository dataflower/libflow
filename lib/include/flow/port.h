#ifndef LIBFLOW_PORT_H
#define LIBFLOW_PORT_H

#include <string>

#include <flow/packet.h>
#include <flow/util/concurrent_queue.h>

namespace flow {

  // A *port* is a generic data connection between two components
  class port {
  
  public:
  
    explicit port (const std::string & name);

    ~port();

    const std::string & get_name();
    
    virtual bool empty() = 0;

  private:

    std::string m_name;
  };


  // An *input_port* receives data from other preocesses
  class input_port : public flow::port {
    
  public:

    // constructor
    explicit input_port (const std::string & name, size_t capacity = 5);
    
    // desctructor
    ~input_port();

    // disable copies
    input_port (const input_port &p) = delete;
    input_port & operator= (const input_port &p) = delete;

    // disable moves
    input_port (input_port &&p) = delete;
    input_port & operator= (input_port &&p) = delete; 

    bool empty();
    bool full();
    
    packet receive();
    void queue (packet &data);

  private:
    util::concurrent_queue<packet> m_queue;
  };

  // An output port sends data to other components
  class output_port : public flow::port {

  public:

    // constructor
    explicit output_port (const std::string & name);
    
    // destructor
    ~output_port();

    // disable copies
    output_port (const output_port &p) = delete;
    output_port & operator= (const output_port &p) = delete;

    // disable moves
    output_port (output_port &&p) = delete;
    output_port & operator= (output_port &&p) = delete; 

    bool empty();
    bool full();

    void send (packet &data);
    
    void connect (input_port &target);
    void disconnect();

  private:
    
    input_port *mp_target;
    bool m_connected;
    
  };

} // namespace flow

#endif // LIBFLOW_PORT_H
