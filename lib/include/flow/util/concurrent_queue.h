#ifndef FLOW_UTIL_CONCURRENT_QUEUE_H
#define FLOW_UTIL_CONCURRENT_QUEUE_H

#include <queue>
#include <mutex>
#include <condition_variable>

namespace flow {
  namespace util {
    
    // A concurrent FIFO queue with fixed capacity 
    //
    // This implementation follows the semantics of a CSP channel:
    // elements are added and removed in FIFO order and the queue blocks 
    // readers when empty and blocks writers when full. 
    // 
    template <typename T>
      class concurrent_queue {
    
    public:
      
      // Constructor
      concurrent_queue (size_t capacity=5);

      // Destructor
      ~concurrent_queue();

      // Get queue capacity
      size_t capacity();
    
      // Get queue size (number of elements in the queue)
      size_t size();
    
      // Check whether the queue is empty
      bool empty();
    
      // Check whether queue is full
      bool full();

      // Access the first element of the queue
      T & front();
    
      // Access the last element of the queue
      T & back();

      // Push a new element into the back of the queue. 
      // Will block if the queue is full.
      void push(T value);
  
      // Pops an element from the front of the queue.
      // Will block if the queue is empty
      T pop();
    
    private:
    
      std::queue<T> m_queue;
      size_t m_capacity;
      std::mutex m_mutex;
      std::condition_variable m_cv_full;
      std::condition_variable m_cv_empty;
    };
    
    template<typename T>
    concurrent_queue<T>::concurrent_queue (size_t capacity):
      m_queue {},
      m_capacity {capacity},
      m_mutex {},
      m_cv_full {},
      m_cv_empty {}
    {
    }
    
    template<typename T>
    concurrent_queue<T>::~concurrent_queue() {
    }

    template <typename T>
      size_t concurrent_queue<T>::capacity() {
      return m_capacity;
    }
    
    template <typename T>
      size_t concurrent_queue<T>::size() {
      return m_queue.size();
    }
    
    template <typename T>
      bool concurrent_queue<T>::empty() {
      return m_queue.empty();
    }

    template <typename T>
      bool concurrent_queue<T>::full() {
      return (m_queue.size() == m_capacity);
    }

    template <typename T>
      T & concurrent_queue<T>::front() {
      return m_queue.front();
    }
    
    template <typename T>
      T & concurrent_queue<T>::back() {
      return m_queue.back();
    }

    template <typename T>
      void concurrent_queue<T>::push(T value) {
      std::unique_lock<std::mutex> qlock (m_mutex);
      m_cv_full.wait (qlock, [&](){ return (m_queue.size() < m_capacity); });
      m_queue.push(std::move (value));
      qlock.unlock();
      m_cv_empty.notify_one();
    }

    template <typename T>  
      T concurrent_queue<T>::pop() {
      std::unique_lock<std::mutex> qlock (m_mutex);
      m_cv_empty.wait (qlock, [&]() { return !m_queue.empty(); });
      T value = std::move(m_queue.front());
      m_queue.pop();
      qlock.unlock();
      m_cv_full.notify_one();
      return value;
    }
    
  } // namespace util
} // namespace flow

#endif // FLOW_UTIL_CONCURRENT_QUEUE_H
