#include <flow/port.h>

namespace flow {

  output_port::output_port (const std::string & name):
    flow::port {name},
    mp_target {nullptr},
    m_connected {false}
  {
  
  }

  output_port::~output_port() {
  
  }

  bool
  output_port::empty() {
    if (m_connected) {
      return mp_target->empty();
    }
    else {
      return true;
    }
  }

  bool 
  output_port::full() {
    if (m_connected) {
      return mp_target->full();
    }
    else {
      return false;
    }
  }

  void 
  output_port::send (packet &data) {
    if (m_connected) {
      mp_target->queue (data);
    }
  }
  
  void 
  output_port::connect (input_port &target) {
    m_connected = true;
    mp_target = &(target);
  }

  void
  output_port::disconnect() {
    m_connected = false;
    mp_target = nullptr;
  }

} // namespace flow
