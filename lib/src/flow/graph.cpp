#include <flow/graph.h>

#include <regex>
#include <thread>

namespace flow {

  graph::graph (const std::string & name): 
    flow::component (name)
  {
  }

  graph::~graph() {
  
  }

  void
  graph::add_component (flow::component & component) {
    m_children[component.get_name()] = &component;
  }

  void
  graph::remove_component (const std::string & name) {
    // TODO
  }

  // FIXME: make return value safe if search fails
  const component &
  graph::get_component (const std::string & name) {
    auto element = m_children.find (name);
    
    return *(element->second);
  }

  unsigned int
  graph::num_components() {
    return m_children.size();
  }

  bool
  graph::connect (const std::string & src_descriptor, 
		  const std::string & tgt_descriptor) {
    std::string src_comp_name, tgt_comp_name;
    std::string src_port_name, tgt_port_name;

    std::tie (src_comp_name, src_port_name) = parse_descriptor (src_descriptor);
    std::tie (tgt_comp_name, tgt_port_name) = parse_descriptor (tgt_descriptor);

    if (src_comp_name.empty() || src_port_name.empty() ||
	tgt_comp_name.empty() || tgt_port_name.empty()) {
      return false;
    }

    auto src_comp_it = m_children.find (src_comp_name);
    auto tgt_comp_it = m_children.find (tgt_comp_name);

    if (src_comp_it == m_children.end() || 
	tgt_comp_it == m_children.end()) {
      return false;
    }

    output_port &src = src_comp_it->second->get_output_port (src_port_name);
    input_port &tgt = tgt_comp_it->second->get_input_port (tgt_port_name);

    // FIXME: make object lookups safe
    src.connect(tgt);
    return true;
  }

  bool
  graph::create_initial_packet (const std::string & tgt_descriptor, 
				flow::packet data) {
    std::string tgt_comp_name, tgt_port_name;

    std::tie (tgt_comp_name, tgt_port_name) = parse_descriptor (tgt_descriptor);
  
    if (tgt_comp_name.empty() || tgt_port_name.empty()) {
      return false;
    }

    auto tgt_comp_it = m_children.find (tgt_comp_name);
    if (tgt_comp_it == m_children.end()) {
      return false;
    }

    input_port &tgt = tgt_comp_it->second->get_input_port (tgt_port_name);

    tgt.queue (data);
    return true;
  }

  void 
  graph::process() {
    std::vector<std::thread> workers;
    
    auto worker_func = [](component *c){
      do{
	c->run();
      } while (c->get_state() != component::state::stopped);
    };
    
    for (auto &child : m_children) {
      workers.push_back (std::thread (worker_func, child.second));
    }

    for (auto &worker : workers) {
      worker.join();
    }
  }

  std::tuple<std::string, std::string> 
  graph::parse_descriptor (const std::string &descriptor_str) {
    
    static const std::regex descriptor_regex ("(\\w+)\\.(\\w+)",
					      std::regex_constants::ECMAScript |
					      std::regex_constants::optimize);

    std::smatch descriptor_match;
    std::string m1, m2;

    if (std::regex_match (descriptor_str, descriptor_match, descriptor_regex)) {
	//&& (descriptor_match.size() == 3)) {
      m1 = descriptor_match[1].str();
      m2 = descriptor_match[2].str();
    }

    return make_tuple(m1, m2);
  }


} // namespace flow
