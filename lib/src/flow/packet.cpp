#include <flow/packet.h>

static const std::string nullstr ("");

namespace flow {

  typedef union _pd {
    int i;
    float f;
    std::string *sp;
  } packet_data;

  flow::packet
  packet::create_data (const std::string &tag, void *data) {
    return packet (packet::type::data, tag, data);
  }

  flow::packet
  packet::create_group_start (const std::string &tag, void *data) {
    return packet (packet::type::group_start, tag, data);
  }

  flow::packet
  packet::create_group_end (const std::string &tag, void *data) {
    return packet (packet::type::group_end, tag, data);
  }

  flow::packet
  packet::create_eos (const std::string &tag, void * data) {
    return packet (packet::type::eos, tag, data);
  }

  packet::packet (packet::type type, const std::string &tag, void * data):
    m_type {type},
    m_tag {tag},
    mp_data {new packet_data}
  {
    if (tag == "s") {
      std::string *sp = static_cast<std::string *>(data); 
      mp_data->sp = new std::string (*sp);
    }
    else if (tag == "i") {
      int *ip = static_cast<int *>(data);
      mp_data->i = *ip;
    }
    else if (tag == "f") {
      float *fp = static_cast<float*>(data);
      mp_data->f = *fp;
    }
  }
  
  // move constructor
  packet::packet (packet &&p):
    m_type {p.m_type},
    m_tag {p.m_tag},
    mp_data {p.mp_data}
  {
    p.m_tag = "";
    p.mp_data = nullptr;
  }

  // move assignment
  packet & 
  packet::operator = (packet &&p) {
    m_type = p.m_type;
    m_tag = p.m_tag;
    mp_data = p.mp_data;
    
    p.m_tag = "";
    p.mp_data = nullptr;
  }

  packet::~packet() {
    if (m_tag == "s") {
      delete mp_data->sp;
    }
    delete mp_data;
  }

  packet::type
  packet::get_type() {
    return m_type;
  }

  const std::string &
  packet::get_tag() {
    return m_tag;
  }

  const std::string &
  packet::get_str (size_t index) {
    if (m_tag[index] == 's') {
      return *(mp_data->sp);
    }
    else return nullstr;
  }

  int
  packet::get_int(size_t index) {
    if (m_tag[index] == 'i') {
      return mp_data->i;
    }
    else return 0;
  }

  float
  packet::get_float (size_t index) {
    if (m_tag[index] == 'f') {
      return mp_data->f;
    }
    else return 0.0f;
  }
  
} // namespace flow
