#include <flow/port.h>

namespace flow {

  port::port (const std::string & name):
    m_name (name)
  {

  }

  port::~port() {
  
  }

  const std::string &
  port::get_name() {
    return m_name;
  }

} // namespace flow
