#include <flow/port.h>

namespace flow {

  input_port::input_port (const std::string & name, size_t capacity):
    flow::port (name),
    m_queue {capacity}
  {
    
  }

  input_port::~input_port () {
  
  }

  bool
  input_port::empty() {
    return m_queue.empty();
  }

  
  bool 
  input_port::full() {
    return m_queue.full();
  }
    
  packet 
  input_port::receive() {
    return m_queue.pop();
  }
  
  void 
  input_port::queue (packet &data) {
    m_queue.push (std::move(data));
  }
} // namespace flow
