#include <flow/component.h>

namespace flow {

  component::component (const std::string & name):
    m_name (name),
    m_state (component::state::ready),
    m_packets(0),
    m_inputs(),
    m_outputs()
  {

  }

  component::~component() {
    
  }

  const std::string & 
  component::get_name() const {
    return m_name;
  }

  component::state
  component::get_state() {
    return m_state;
  }

  input_port &
  component::get_input_port (const std::string &port_name) {
    auto element = m_inputs.find(port_name);

    return *(element->second);
  }

  output_port &
  component::get_output_port (const std::string &port_name) {
    auto element = m_outputs.find(port_name);
    return *(element->second);
  }

  void 
  component::run() {
    m_state = component::state::running;
    process();
    if (m_state != component::state::stopped) {
      m_state = component::state::ready;
    }
  }

  void 
  component::stop() {
    m_state = component::state::stopped;
    cleanup();
  }
  
  void
  component::create_input (flow::input_port & port) {
    m_inputs[port.get_name()] = &port;
  }

  void
  component::create_output (flow::output_port & port) {
    m_outputs[port.get_name()] = &port;
  }

  flow::packet
  component::receive_packet (flow::input_port & port) {
    m_state = component::state::blocked;
    packet p = port.receive();
    ++m_packets;
    m_state = component::state::running;
    return p;
  }

  void
  component::send_packet (flow::output_port & port, flow::packet &p) {
    m_state = component::state::blocked;
    port.send (p);
    --m_packets;
    m_state = component::state::running;
  }

  void 
  component::send_eos (flow::output_port & port) {
    m_state = component::state::blocked;
    packet p = packet::create_eos();
    port.send (p);
    m_state = component::state::running;
  }

  flow::packet
  component::create_packet (std::string & data) {
    return packet::create_data ("s", &data);
    ++m_packets;
  }

  flow::packet
  component::create_packet (int data) {
    return packet::create_data ("i", &data);
    ++m_packets;
  }

  flow::packet
  component::create_packet (float data) {
    return packet::create_data ("f", &data);
    ++m_packets;
  }

  void
  component::drop_packet (flow::packet &packet) {
    --m_packets;
  }

} // namespace flow
