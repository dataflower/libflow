#include <unittest++/UnitTest++.h>

#include <thread>

#include <flow/port.h>

using namespace flow;

SUITE(FlowPort)
{
  
  TEST(CreateInputPort)
  {
    input_port iport ("TEST");

    CHECK("TEST" == iport.get_name());
  }

  TEST(InputPortData)
  {
    input_port iport ("IN");
    
    std::string sdata ("data");
    packet p1 = packet::create_data ("s", &sdata);
    iport.queue (p1);
    
    packet p2 = iport.receive();
    CHECK(p2.get_str(0) == sdata);
  }

  TEST(CreateOutputPort)
  {
    output_port oport ("OUT");
    
    CHECK("OUT" == oport.get_name());
  }

  TEST(PortConnection)
  {
    std::string sdata ("data");
    packet pp = packet::create_data ("s", &sdata);
    input_port iport("IN");
    output_port oport("OUT");
    
    oport.connect(iport);

    std::thread producer ([&]() {
	oport.send(pp);
      });
    
    std::thread consumer ([&]() {
	packet pc = iport.receive();
	
	CHECK(pc.get_str(0) == sdata);
      });
  
    producer.join();
    consumer.join();
  }

} // SUITE(FlowPort)

