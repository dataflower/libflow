#include <unittest++/UnitTest++.h>

#include <flow/packet.h>
#include <flow/graph.h>

#include <iostream>

using namespace flow; 

// define components
class text_gen : public flow::component {
  
private:
  
  flow::input_port m_options;
  flow::output_port m_output;
  std::string m_text;

public:

  text_gen (std::string name):
    flow::component (name),
    m_options ("OPTIONS"),
    m_output ("OUT"),
    m_text ("default generated text") 
  {
    create_input (m_options);
    create_output (m_output);
  }
  
  ~text_gen() {}
  
  void process() {
    if (not m_options.empty()) {
      packet opt = receive_packet (m_options);
      m_text = opt.get_str(0);
      drop_packet (opt);
    }
    
    packet p = create_packet (m_text);
    send_packet (m_output, p);
    send_eos (m_output);
    stop();
  }
};


class console_display : public flow::component {
  
private:
  
  flow::input_port m_input;

public:
  
  bool m_check;

  console_display (std::string name):
    flow::component {name},
    m_input {"IN"},
    m_check {false}
  {
    create_input (m_input);
  }
  
  ~console_display() {}
  
  void process() 
  {
    if (m_input.empty()) return;
    flow::packet p = receive_packet (m_input);
    if (p.get_type() == packet::type::eos) {
      drop_packet(p);
      stop();
      return;
    }
    
    std::string str = p.get_str(0);
    if (str == "Hello World!") {
      m_check = true;
    }
    
    // string display is not needed for automated testing
    // std::cout << str << std::endl;
    
    drop_packet (p);
  }
};

SUITE(FlowGraph)
{

  TEST(CreateGraph)
  {
    graph g ("test");
    
    CHECK(g.get_name() == "test");
  }

  TEST(AddComponents)
  {
    graph g ("test");
    text_gen generator ("generator");    

    g.add_component (generator);

    const component &comp =  g.get_component("generator");
    CHECK(comp.get_name() == "generator");
    CHECK_EQUAL(1, g.num_components());
  }

  TEST(RemoveComponents)
  {
  }

  TEST(ListComponents)
  {
  }

  TEST(RunGraph)
  {
    text_gen generator ("generator");
    console_display display ("display");

    graph g ("helloworld");

    g.add_component (generator);
    g.add_component (display);

    CHECK(g.connect ("generator.OUT", "display.IN"));
    std::string data ("Hello World!");
    g.create_initial_packet ("generator.OPTIONS", 
			     packet::create_data ("s", &data));

    g.run();

    CHECK(display.m_check);
  }

} // SUITE(FlowGraph)
