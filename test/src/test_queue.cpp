#include <unittest++/UnitTest++.h>

#include <chrono>
#include <thread>

#include <flow/util/concurrent_queue.h>

using namespace flow;

SUITE(ConcurrentQueue)
{
  TEST(CreateDefaultQueue)
  {
    util::concurrent_queue<int> queue;
    
    CHECK_EQUAL(5, queue.capacity());
    CHECK(queue.empty());
  }

  TEST(CreateQueueWithCapacity)
  {
    util::concurrent_queue<int> queue(3);
    
    CHECK_EQUAL(3, queue.capacity());
    CHECK(queue.empty());
  }

  TEST(QueuePush) 
  {
    util::concurrent_queue<int> queue(3);
    
    queue.push(1);
    queue.push(2);
    queue.push(3);
    
    CHECK_EQUAL(1, queue.front());
    CHECK_EQUAL(3, queue.back());
    CHECK_EQUAL(3, queue.size());
    CHECK(queue.full());
  }

  TEST(QueuePop) 
  {
    util::concurrent_queue<int> queue(3);

    queue.push(1);
    queue.push(2);
    queue.push(3);

    CHECK_EQUAL(1, queue.pop());
    CHECK_EQUAL(2, queue.pop());
    CHECK_EQUAL(3, queue.pop());
    CHECK(queue.empty());
  }

  TEST(ConcurrentAccessFastWriterSlowReader)
  {
    util::concurrent_queue<int> queue(3);

    std::thread producer ([&queue]() {
  	queue.push(1);
  	queue.push(2);
  	queue.push(3);
  	queue.push(4);
  	queue.push(5);
      });

    std::thread consumer ([&queue]() {
    	std::chrono::milliseconds delay (100);
    	CHECK_EQUAL(1, queue.pop());
    	std::this_thread::sleep_for (delay);
    	CHECK_EQUAL(2, queue.pop());
    	std::this_thread::sleep_for (delay);
    	CHECK_EQUAL(3, queue.pop());
    	std::this_thread::sleep_for (delay);
    	CHECK_EQUAL(4, queue.pop());
    	std::this_thread::sleep_for (delay);
    	CHECK_EQUAL(5, queue.pop());
    	std::this_thread::sleep_for (delay);
      });

    {
      UNITTEST_TIME_CONSTRAINT(10000);
      producer.join();
      consumer.join();
    }
    
  }

  TEST(ConcurrentAccessSlowWriterFastReader)
  {
    util::concurrent_queue<int> queue(3);

    std::thread producer ([&queue]() {
  	std::chrono::milliseconds delay (100);
  	queue.push(1);
  	std::this_thread::sleep_for (delay);
  	queue.push(2);
  	std::this_thread::sleep_for (delay);
  	queue.push(3);
  	std::this_thread::sleep_for (delay);
  	queue.push(4);
  	std::this_thread::sleep_for (delay);
  	queue.push(5);
  	std::this_thread::sleep_for (delay);
      });
    
    std::thread consumer ([&queue]() {
  	CHECK_EQUAL(1, queue.pop());
  	CHECK_EQUAL(2, queue.pop());
  	CHECK_EQUAL(3, queue.pop());
  	CHECK_EQUAL(4, queue.pop());
  	CHECK_EQUAL(5, queue.pop());
      });

    {
      UNITTEST_TIME_CONSTRAINT(10000);
      producer.join();
      consumer.join();
    }
  }

} // SUITE(ConcurrentQueue)
