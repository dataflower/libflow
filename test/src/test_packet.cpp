#include <unittest++/UnitTest++.h>

#include <flow/packet.h>

using namespace flow;

SUITE(FlowPacket)
{
  TEST (CreateData)
  {
    std::string sdata ("hello");
    packet sp = packet::create_data ("s", &sdata);
    CHECK(sp.get_type() == packet::type::data);
    CHECK(sp.get_tag() == "s");
    CHECK(sp.get_str(0) == sdata);    
    
    int idata = 42;
    packet ip = packet::create_data ("i", &idata);
    CHECK(ip.get_type() == packet::type::data);
    CHECK(ip.get_tag() == "i");
    CHECK(ip.get_int(0) == idata);    

    float fdata = 3.14;
    packet fp = packet::create_data ("f", &fdata);
    CHECK(fp.get_type() == packet::type::data);
    CHECK(fp.get_tag() == "f");
    CHECK(fp.get_float(0) == fdata);
  }

  TEST (CreateGroupStart)
  {
    packet p = packet::create_group_start();
    
    CHECK(p.get_type() == packet::type::group_start);
  }

  TEST(CreateGroupEnd)
  {
    packet p = packet::create_group_end();
    
    CHECK(p.get_type() == packet::type::group_end);
  }

  TEST(CreateEos)
  {
    packet p = packet::create_eos();
    
    CHECK(p.get_type() == packet::type::eos);
  }

}; // SUITE(FlowPacket) 
