#include <unittest++/UnitTest++.h>

#include <flow/component.h>

#include <iostream>
#include <thread>

using namespace flow;

class TestSourceComponent : public component {

  output_port m_output;

public:

  TestSourceComponent(const std::string & name): 
    component {name},
    m_output {"OUT"}  
  {
    create_output (m_output);
  }

  ~TestSourceComponent() {
  }

  void process() {
    int counter = 0;
    while (counter < 10) {
      packet p = create_packet (counter);
      send_packet (m_output, p);
      ++counter;
    }

    send_eos (m_output);
    stop();
  };

};

class TestFilterComponent : public component {
  
  input_port m_input;
  output_port m_output;

public:

  TestFilterComponent(const std::string & name):
    component {name},
    m_input {"IN"},
    m_output {"OUT"}
  {
    create_input (m_input);
    create_output (m_output);
  }

  ~TestFilterComponent() {
  }

  void process() {
    packet pin = receive_packet (m_input);
    if (pin.get_type() == packet::type::eos) {
      drop_packet (pin);
      stop();
      return;
    }

    packet pout = create_packet (pin.get_int(0) * 2);
    
    drop_packet (pin);
    send_packet (m_output, pout);
  }
};

class TestSinkComponent : public component {
  
  input_port m_input;

public:

  TestSinkComponent(const std::string & name):
    component {name},
    m_input {"IN"}
  {
    create_input (m_input);
  }
  
  ~TestSinkComponent() {
  }

  void process() {
    int counter = 0;
    while (counter < 10) {
      packet p = receive_packet (m_input);      
      CHECK_EQUAL(counter * 2, p.get_int(0));
      drop_packet(p);
      ++counter;
    }
    stop();
  }

};


SUITE(FlowComponent)
{

  TEST(CreateComponent)
  {
    TestSourceComponent comp ("source");
    
    CHECK(comp.get_name() == "source");
  }

  TEST(ConnectAndRunComponents)
  {
    TestSourceComponent source ("source");
    TestFilterComponent filter ("filter");
    TestSinkComponent sink ("sink");
  
    source.get_output_port("OUT").connect(filter.get_input_port("IN"));
    filter.get_output_port("OUT").connect(sink.get_input_port("IN"));
  
    auto runner = [](component *c) {
      do {
	c->run();
      } while (c->get_state() != component::state::stopped);
      
    };
    
    std::thread w1 (runner, &source);
    std::thread w2 (runner, &filter);
    std::thread w3 (runner, &sink);

     w1.join();
     w2.join();
     w3.join();
  }
}
