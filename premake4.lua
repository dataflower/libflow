solution "libflow"
location "build"
configurations {"Debug", "Release"}

-- build libflow
project "flow"
location "build"
targetdir "build"
kind "StaticLib"
language "C++"
files { "lib/src/**.cpp" }
buildoptions { "-std=c++11", "-pthread" }
includedirs { "lib/include" }

configuration "Debug"
flags {"ExtraWarnings", "Symbols"}

-- build tests
local testlib_path = "test/lib/libUnitTest++.a" 

if os.isfile (testlib_path) then
   testlib_path = "../" .. testlib_path
else
   testlib_path = os.findlib ("libUnitTest++.a")
   if testlib_path then
      testlib_path = testlib_path .. "libUnitTest++.a"
   end
end

if testlib_path then
   project "runtests"
   location "build"
   targetdir "build"
   kind "ConsoleApp"
   language "C++"
   files { "test/src/**.cpp" }
   includedirs { "lib/include" }
   buildoptions { "-std=c++11" }
   links { "flow" }
   linkoptions { testlib_path, "-pthread" }
   postbuildcommands { "$(TARGET)" }

   configuration "Debug"
   flags {"ExtraWarnings", "Symbols"}
else
  print "UnitTest++ library not found. Tests will not be built"
end
